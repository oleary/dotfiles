# dotfiles

These are my personal dotfiles. Use at your own risk :wink:

## Usage
* Clone with `git clone git@gitlab.com:brendan/dotfiles.git ~/.dotfiles`
* Symlink all the files to ~/
* This can be automated with https://gitlab.com/brendan/formation